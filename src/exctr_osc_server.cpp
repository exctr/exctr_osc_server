/*

	This file is part of exctr.

  Copyright (C) 2014  Jari Suominen

  exctr_osc_server is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  exctr_osc_server is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with exctr.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
  *** OSC API ***
  ***************

  (s) = string (f.e. aweirdsentence)
  (i) = integer (f.e. 666)
  (f) = float (f.e. 0.666)

  For instance an example OSC message of the:
  /exctr/volume node(i) volume(i)
  would be:
  /exctr/volume 2 127
  where we are sending volume 127 (full) to node with ID# 2.
  

  *********************************
  NODE SPECIFIC OSC MESSAGES ******    
  *********************************

  /exctr/initialize
    Will trigger address initializing sequence of the nodes.
    Nodes of the net might not have addresses assigned to them before this is called.

  /exctr/frequency node(i) frequency(i)
    Will set a frequency [frequency] of node with an address [node].
    node = address of the node
    frequency = Hz value in integer [0,8000] Hz.

  /exctr/volume node(i) volume(i)
    Sets a volume of the given node.
    node = address of the node
    volume = volume of the node [0,127]

  /exctr/master_volume node(i) volume(i)
    Sets an output volume of the given node.
    node = address of the node
    volume = volume of the node [0,127]

  /exctr/frequency_portamento node(i) speed(i)
    Sets frequency portamento speed (the speed frequency travels from one value to another)
    speed =
      0: no fade, frequency jumps immedately to the destination value
      1024: no fade, frequency stays in current value
      [1,1023]: frequency travels slowly towards destination value.
                smaller values, faster shift.

  /exctr/volume_portamento node(i) speed(i)
    Sets volume fade speed (the speed volume fades from one value to another).
    speed = volume units / 2 / millisecond, f.e. 10 will fade from silence to full blast
      in 2.5 seconds while 1000 will take 4 minutes 15 seconds:
      0: no fade, volume jumps immedately to the destination value
      1024: no fade, volume stays in current value
      [1,1023]: volume fades slowly towards destination value.
                smaller values, faster fade.

  /exctr/frequency_volume node(i) frequency(i) volume(i)
    Sets a frequency and volume of the given node.

  /exctr/waveform node(i) waveform(i)
    Sets a waveform of the given node.
      waveform = 
        0: square
        1: saw
        2: triangle
        3: noise
        4: sine
        5: colored noise (adjust noise color by setting the frequency of the node)
        6: band-limited saw (experimental)
        7: band-limited square (highly experimental) 

  /exctr/lp node(i) on(i)
    Sets on/off 1st order LP filter
    on =
      0 filter off
      1 filter on

  /exctr/LED node(i) LED(i) pwm(i)
    Sets the brightness of the LED connected to the node.
    LED = {0,1} the number of the LED. Most boards have only LED #1 connected.
    pwm = [0,127] brightness of the LED. 127 is the brightest.


  ************************
  BROADCAST MESSAGES *****
  ************************

  Most node specific messages will have their broadcast counterpart soonish (NOT YET IMPLEMENTED).
  They all work similarly than their single node counterparts, but as they will be sent to all
  the nodes of the network, the ID of the node is not sent. Refer to the documentation of the
  single node version.

  /exctr/broadcast/frequency frequency(i) *
  /exctr/broadcast/volume volume(i) *
  /exctr/broadcast/master_volume volume(i) *
  /exctr/broadcast/frequency_portamento speed(i) *
  /exctr/broadcast/volume_portamento speed(i) *
  /exctr/broadcast/waveform waveform(i) *
  /exctr/broadcast/lp on(i)
  /exctr/broadcast/LED LED(i) pwm(i) *

 
  **********************************
  SERVER RELATED OSC MESSAGES ******    
  **********************************

  /exctr/global/mastervolume volume(f)
    Will scale the volume messages sent out from the server.
    Will also update volume of the nodes when the value is changed.
    In practice will work as master fader in the mixing table.
    To put it more clearly (?), the volume relations of the nodes will
    stay as they were.
    NOTE: as the message will start up the process where hundreds
          serial bytes are sent out, it is not smart to send this
          more often than once per 250ms.
    volume = a floating point number between [0,1]

  /exctr/ping ip(s) port(s)
    Send ping to the address specified. Don't send the address of the server here,
    will most likely result to the indifinite loop.
    ip = ip-address where ping is pinged (f.e. 192.168.100.666)
    port = port where ping is pinged (f.e. 6666)

  /exctr/record filename(s)
    Start to record OSC messages the server is receaving to a file.
    filename = full path of the filesystem where server is running

  /exctr/stop_recording
    Will stop recording OSC messages and close the file.

*/

/*

                             _        _
                         _.-'/   _.:'`/
                      ,'`   ( ,:;.-'`(
                    .'      .:'`      \
                   /       //    _.-';)
                 _/      _//_.-;:-'``/
          //|    \      \  .-'`      \                                 ,
         || /_,-,_|      | `""--..__  \                  .-'```'-.     )\
    _.--'_  '-;_/_)_     |(``""'---.;"/-,.-.  _         /  .---.  \  .'  \
   /6    ^`     ':_/     | "-._    .-'../__ )' ',.-. _ |  /     \  ;/_  _/
  (`-----`--'.    \_)    ;|`"-.;-./        `""--;.__) ',-.      (| |  ||
 __)         {\   |_/\   \\    _.'                  `"-;_ )'-,_(`/ ;_.'/
  /         {=|   |)  \.-"\\  /                          `'-.;_:'  /_.'
            {=|    \_.'    )) '        /                          /
            {=|     ,                 |      ,                _.-'
             {=;     `""--.            \    '.       __,.---'`
              {=\          `\           '._   '._.-"`
   jgs  _,.--"`;{\ '-.._    /        __,..-'-._ '.
       (((/==)/ _`;.--'"` .'--""""```  .--"```    )
        ```  ' (((/====```            ((((/======'
                ```                    ```

*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <argp.h>
#include <fcntl.h>
#include <signal.h>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <sys/stat.h>

#include <lo/lo.h>

#include <linux/serial.h>
#include <linux/ioctl.h>
#include <asm/ioctls.h>
#include <termios.h>

#include "boost/date_time/posix_time/posix_time.hpp"
namespace pt = boost::posix_time;

#define OUTPUT_BUFFER_SIZE 1024

#define MAX_DEV_STR_LEN               32
#define MAX_MSG_SIZE                1024
#define MAX_SERIAL_DEVICE_COUNT 8

using namespace std;

/* change this definition for the correct port */
//#define _POSIX_SOURCE 1 /* POSIX compliant source */

bool run;
int serials[MAX_SERIAL_DEVICE_COUNT];
int volumes[MAX_SERIAL_DEVICE_COUNT*128];
double mastervolume = 1.0;
struct termios oldtio, newtio;
bool recording = false;
pt::ptime recordingstarted;
ofstream savefile;


/* --------------------------------------------------------------------- */
// Program options

static struct argp_option options[] = 
{
  {"ip", 'i', "IP", 0, "IP address (or URL, for example foo.dyndns.org) where OSC messages are sent. Default = 127.0.0.1" },
  {"sport", 'p', "SEND_PORT", 0, "Port where OSC messages are sent. Default = 7000" },
  {"rport", 'r', "RECEIVING_PORT", 0, "Port for incoming OSC traffic. Default is port defined with -p" },
  {"name", 'n', "NAME", 0, "Name of Alsa MIDI client created. Default = oscmidi" },
  {"serialdevice", 's', "DEV" , 0, "Serial device to use. Default = /dev/ttyACM0" },
  {"baudrate", 'b', "BAUD", 0, "Serial port baud rate. Default = 115200" },
  {"verbose", 'v', 0, 0, "For debugging: Produce verbose output" },
  {"respawn", 'w', 0, 0, "Will try to recope if the serial device is not present" },
  {"tcp", 't', 0, 0, "Will use TCP instead of UDP" },
  {"serialcount", 'c', "SERIALPORT_COUNT", 0, "How many serial devices this server will control" },
  { 0 }
};

typedef struct _arguments
{
  int verbose, respawn;
  int tcp;
  char ip[MAX_DEV_STR_LEN];
  char name[MAX_DEV_STR_LEN];
  char port[MAX_DEV_STR_LEN];
  char rport[MAX_DEV_STR_LEN];
  int serialcount;
  char serialdevice[MAX_DEV_STR_LEN];
  int  baudrate;
} arguments_t;


static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	arguments_t *arguments = (arguments_t*)(state->input);
	
	switch (key)
	{
		case 'v':
			arguments->verbose = 1;
			break;
		case 'w':
			arguments->respawn = 1;
			break;
    case 't':
			arguments->tcp = 1;
			break;
		case 'i':
			if (arg == NULL) break;
			strncpy(arguments->ip, arg, MAX_DEV_STR_LEN);
			break;
		case 'n':
			if (arg == NULL) break;
			strncpy(arguments->name, arg, MAX_DEV_STR_LEN);
			break;
		case 'p':
			if (arg == NULL) break;
            strncpy(arguments->port, arg, MAX_DEV_STR_LEN);
			break;
		case 'r':
			if (arg == NULL) break;
			 strncpy(arguments->rport, arg, MAX_DEV_STR_LEN);
			break;
		case 's':
			if (arg == NULL) break;
			strncpy(arguments->serialdevice, arg, MAX_DEV_STR_LEN);
			break;	
		case 'c':
			if (arg == NULL) break;
			arguments->serialcount = strtol(arg, NULL, 0);
			break;	
		case ARGP_KEY_ARG:
		case ARGP_KEY_END:
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void arg_set_defaults(arguments_t *arguments)
{
  char *ip_temp = (char *)"127.0.0.1";
  char *serialdevice_temp = (char *)"/dev/ttyEXCTR0";
  char *port_temp = (char *)"7000";
  char *rport_temp = (char *)"";
	strncpy(arguments->ip, ip_temp, MAX_DEV_STR_LEN);
  strncpy(arguments->port, port_temp, MAX_DEV_STR_LEN);
  strncpy(arguments->rport, rport_temp, MAX_DEV_STR_LEN);
  strncpy(arguments->serialdevice, serialdevice_temp, MAX_DEV_STR_LEN);

  arguments->baudrate     = B115200;
	arguments->serialcount = 1;

	arguments->verbose      = 0;
  arguments->tcp      = 0;
	arguments->respawn = 0;

	char *name_temp = (char *)"exctr_osc_server";
	strncpy(arguments->name, name_temp, MAX_DEV_STR_LEN);	
}

const char *argp_program_version     = "exctr_osc_server x666x";
const char *argp_program_bug_address = "satan@heaven.mil";
static char doc[]       = "exctr_osc_server";
static struct argp argp = { options, parse_opt, 0, doc };
arguments_t arguments;


void exit_cli(int sig)
{
	run = false;
}


void error(int num, const char *msg, const char *path)
{
    printf("liblo server error %d in path %s: %s\n", num, path, msg);
    fflush(stdout);
}


string convertInt(int number)
{
    if (number == 0)
      return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}

int exctr_write(int a, char * b, int n) {  
  int result = -1;
	int subnet = a/128;
	if (subnet < arguments.serialcount) {
		b[1] = a - subnet * 128;
		if (serials[subnet] > 0) {
			result = write(serials[subnet],b,n);
			tcflush(serials[subnet], TCIFLUSH);
		}
	}
	return result;
}

int exctr_volume(const char *path, const char *types, lo_arg ** argv,
                    int argc, void *data, void *user_data) {
  if (savefile.is_open()) {
    savefile << argv[0]->i << endl;
  }  
  return 0;
}

int process_incoming_osc(const char *path, const char *types, lo_arg ** argv,
                    int argc, void *data, void *user_data) {

	int result;
  try {

    if (arguments.verbose)
      cout << pt::microsec_clock::local_time();  

    if( std::strcmp( path, "/exctr/record" ) == 0 && argc == 1) {
      if (savefile.is_open())
        savefile.close();
      savefile.open (&argv[0]->s);
      recordingstarted = pt::microsec_clock::local_time();
      if (arguments.verbose)
        cout << "\t/exctr/record filename=" << &argv[0]->s << endl;             
    }
    else if( std::strcmp( path, "/exctr/stop_recording" ) == 0 && argc == 0) {
      savefile.close();
      if (arguments.verbose)
        cout << "\t/exctr/stop_recording" << endl;             
    }
    else {
      if( std::strcmp( path, "/exctr/frequency" ) == 0 && argc == 2) {
        char bytes[] = {0xF0, argv[0]->i, (argv[1]->i) & 0x7F, (argv[1]->i) >> 7};
  			exctr_write(argv[0]->i, bytes, 4);
        if (arguments.verbose)
          cout << "\t/exctr/frequecy node=" << argv[0]->i << " frequency=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/frequency" ) == 0 && argc == 1) {
        char bytes[] = {0xF0, 127, (argv[0]->i) & 0x7F, (argv[0]->i) >> 7};
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 4);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/frequency frequency=" << argv[0]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/volume" ) == 0 && argc == 2) {
        char bytes[] = { 0xF1, argv[0]->i, char((argv[1]->i)*mastervolume) }; 
        exctr_write(argv[0]->i, bytes, 3);
  			if (arguments.verbose)
          cout << "\t/exctr/volume node=" << argv[0]->i << " volume=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/master_volume" ) == 0 && argc == 2) {
        char bytes[] = { 0xF2, argv[0]->i, argv[1]->i }; 
        exctr_write(argv[0]->i, bytes, 3);
  			if (arguments.verbose)
          cout << "\t/exctr/master_volume node=" << argv[0]->i << " volume=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/volume" ) == 0 && argc == 1) {
        char bytes[] = {0xF1, 127, argv[0]->i };
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 3);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/volume volume=" << argv[0]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/master_volume" ) == 0 && argc == 1) {
        char bytes[] = {0xF2, 127, argv[0]->i };
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 3);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/master_volume volume=" << argv[0]->i << endl;               
      }
      else if (std::strcmp( path, "/exctr/initialize" ) == 0 && argc == 0) {
        char bytes[] = {0xF3, 0x00};
  			for (int i = 0; i < arguments.serialcount; i++) { 
        	write(serials[i], bytes, 2);
  				tcflush(serials[i], TCIFLUSH);
  			}
        if (arguments.verbose)
          cout << "\tInitializing!" << endl;             				
      }
      else if( std::strcmp( path, "/exctr/waveform" ) == 0 && argc == 2) {
        char bytes[] = {0xF5, argv[0]->i, argv[1]->i}; 
        exctr_write(argv[0]->i, bytes, 3);
        if (arguments.verbose)
          cout << "\t/exctr/waveform node=" << argv[0]->i << " waveform=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/waveform" ) == 0 && argc == 1) {
        char bytes[] = {0xF5, 127, argv[0]->i };
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 3);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/waveform waveform=" << argv[0]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/LED" ) == 0 && argc == 3) {
        char bytes[] = {0xF6, argv[0]->i, argv[1]->i, argv[2]->i}; 
        exctr_write(argv[0]->i, bytes, 4);
        if (arguments.verbose)
          cout << "\t/exctr/LED node=" << argv[0]->i << " led=" << argv[1]->i << " brightness=" << argv[2]->i << endl;             
  		}
      else if( std::strcmp( path, "/exctr/broadcast/LED" ) == 0 && argc == 2) {
        char bytes[] = {0xF6, 127, argv[0]->i , argv[1]->i};
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 4);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/LED led=" << argv[0]->i << " brightness=" << argv[1]->i << endl;               
      }
  		else if( std::strcmp( path, "/exctr/volume_portamento" ) == 0 && argc == 2) {
        char bytes[] = {0xF7, argv[0]->i, (argv[1]->i) & 0x7F, (argv[1]->i) >> 7}; 
        exctr_write(argv[0]->i, bytes, 4);
        if (arguments.verbose)
          cout << "\t/exctr/volume_portamento node=" << argv[0]->i << " speed=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/volume_portamento" ) == 0 && argc == 1) {
        char bytes[] = {0xF7, 127, (argv[0]->i) & 0x7F, (argv[0]->i) >> 7};
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 4);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/volume_portamento speed=" << argv[0]->i << endl;             
      }
  		else if( std::strcmp( path, "/exctr/frequency_portamento" ) == 0 && argc == 2) {
        char bytes[] = {0xF8, argv[0]->i, (argv[1]->i) & 0x7F, (argv[1]->i) >> 7}; 
        exctr_write(argv[0]->i, bytes, 4);
        if (arguments.verbose)
          cout << "\t/exctr/frequency_portamento node=" << argv[0]->i << " speed=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/frequency_portamento" ) == 0 && argc == 1) {
        char bytes[] = {0xF8, 127, (argv[0]->i) & 0x7F, (argv[0]->i) >> 7};
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 4);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/frequency_portamento speed=" << argv[0]->i << endl;             
      }
  		else if( std::strcmp( path, "/exctr/lp" ) == 0 && argc == 2) {
        char bytes[] = {0xF9, argv[0]->i, argv[1]->i}; 
        exctr_write(argv[0]->i, bytes, 3);
  			if (arguments.verbose)
          cout << "\t/exctr/lp node=" << argv[0]->i << " on=" << argv[1]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/broadcast/lp" ) == 0 && argc == 1) {
        char bytes[] = {0xF9, 127, argv[0]->i};
        for (int i = 0; i < arguments.serialcount; i++) {
          if (serials[i]!=-1) {
        	  write(serials[i], bytes, 3);
  				  tcflush(serials[i], TCIFLUSH);
          }
  			}    
  			if (arguments.verbose)
          cout << "\t/exctr/broadcast/lp on=" << argv[0]->i << endl;             
      }
  		else if( std::strcmp( path, "/exctr/frequency_volume" ) == 0 && argc == 3) {
        volumes[argv[0]->i] = argv[2]->i;
  			char bytes[] = {0xF4, argv[0]->i, (argv[1]->i) & 0x7F, (argv[1]->i) >> 7, char((argv[2]->i)*mastervolume)}; 
  			exctr_write(argv[0]->i, bytes, 5);
  			if (arguments.verbose)
  				cout << "\t/exctr/frequency_volume node=" << argv[0]->i << " frequency=" << argv[1]->i << " volume=" << argv[2]->i << endl;             
      }
      else if( std::strcmp( path, "/exctr/ping" ) == 0 && argc == 2) {
  			if (arguments.verbose)
  				cout << "\t/exctr/ping from [" << &argv[0]->s << ":" << &argv[1]->s << "]" << endl; 
        lo_address ts;
        if ( std::strcmp( &argv[0]->s, arguments.ip ) != 0  || std::strcmp( &argv[1]->s, arguments.rport ) != 0) {
          if (arguments.tcp)
            ts = lo_address_new_with_proto(LO_TCP, &argv[0]->s, &argv[1]->s);
          else
	          ts = lo_address_new(&argv[0]->s, &argv[1]->s);
                    
          lo_send(ts, "/exctr/ping", "ss", &argv[0]->s, &argv[1]->s);
        }
      }
  		else {
        if (arguments.verbose)
			    std::cout << "\tOSC received '" << path << "' message, which is not exctr-message!" << endl;
        return 1;
	    }

      if (savefile.is_open()) {
        savefile << pt::microsec_clock::local_time()-recordingstarted << " " << path << " " << types ;
        recordingstarted = pt::microsec_clock::local_time();
        for (int i = 0; i < argc; i++) {
          if (types[i] == 'i')
            savefile << " " << (int)argv[i]->i;
          else
            savefile << " " << &argv[i]->s;
        }
        if (argc==0)
          savefile << "x";
        savefile << endl;
      }
    }       
  } catch ( ... ) {
    if (arguments.verbose)
      std::cout << "error while parsing message: " <<  "\n";
  } 
  return 1;
}

int open_serial_device(string path) {

	/* 
	 *  Open modem device for reading and not as controlling tty because we don't
	 *  want to get killed if linenoise sends CTRL-C.
	 */
	
	int s = open(path.c_str(), O_RDWR | O_NOCTTY ); 

  if (s < 0) 
	{
		return -1;
	}

	/* save current serial port settings */
	tcgetattr(s, &oldtio); 

	/* clear struct for new port settings */
	bzero(&newtio, sizeof(newtio)); 

	/* 
	 * BAUDRATE : Set bps rate. You could also use cfsetispeed and cfsetospeed.
	 * CRTSCTS  : output hardware flow control (only used if the cable has
	 * all necessary lines. See sect. 7 of Serial-HOWTO)
	 * CS8      : 8n1 (8bit, no parity, 1 stopbit)
	 * CLOCAL   : local connection, no modem contol
	 * CREAD    : enable receiving characters
	 */
	//newtio.c_cflag = arguments.baudrate | CS8 | CLOCAL | CREAD; // CRTSCTS removed
	newtio.c_cflag = arguments.baudrate | CS8 | CLOCAL; // CRTSCTS removed

	/*
	 * IGNPAR  : ignore bytes with parity errors
	 * ICRNL   : map CR to NL (otherwise a CR input on the other computer
	 * will not terminate input)
	 * otherwise make device raw (no other input processing)
	 */
	newtio.c_iflag = IGNPAR;

	/* Raw output */
	newtio.c_oflag = 0;

	/*
	 * ICANON  : enable canonical input
	 * disable all echo functionality, and don't send signals to calling program
	 */
	newtio.c_lflag = 0; // non-canonical

	/* 
	 * set up: we'll be reading 4 bytes at a time.
	 */
	newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
	newtio.c_cc[VMIN]     = 1;     /* blocking read until n character arrives */

	/* 
	 * now clean the modem line and activate the settings for the port
	 */
	tcflush(s, TCIFLUSH);
	tcsetattr(s, TCSANOW, &newtio);

	// Linux-specific: enable low latency mode (FTDI "nagling off")
  //	ioctl(serial, TIOCGSERIAL, &ser_info);
  //	ser_info.flags |= ASYNC_LOW_LATENCY;
  //	ioctl(serial, TIOCSSERIAL, &ser_info);

	if (arguments.verbose)
		cout << path << " (" << s << "): connected!" << endl;

	return s;
	
}


/* --------------------------------------------------------------------- */
// Main program

main(int argc, char** argv)
{

	/* 
	 * read command line arguments
	 */
	arg_set_defaults(&arguments);
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	if (arguments.verbose)
		cout << endl << "Exctr OSC Server  Copyright (C) 2014  Jari Suominen" << endl
			<< "This program comes with ABSOLUTELY NO WARRANTY." << endl
			<< "This program is free software: you can redistribute it and/or modify" << endl
			<< "it under the terms of the GNU General Public License as published by" << endl
			<< "the Free Software Foundation, either version 3 of the License, or" << endl
			<< "(at your option) any later version." << endl;

	
	size_t last_index = string(arguments.serialdevice).find_last_not_of("0123456789");
	string serialdevicebody = string(arguments.serialdevice).substr(0,last_index+1);
	int serialdevicenumber = strtol(string(arguments.serialdevice).substr(last_index+1).c_str(),NULL,0);

  for (int i = 0; i < sizeof(volumes) / sizeof (int); i++) {
    volumes[i] = 0;
  }

	for (int i = 0; i < arguments.serialcount; i++) {

		string fullpath;
		fullpath += serialdevicebody;
		fullpath += convertInt(i+serialdevicenumber);

		serials[i] = open_serial_device(fullpath);

		if (serials[i] < 0) {
			if (arguments.respawn) {
				if (arguments.verbose)
					cout << fullpath << ": not present at the moment."  << endl;			
			}
			else {	
				perror(fullpath.c_str());
				exit(-1);
			}
		}
	}	

    /*
     * OSC In
     */
	if (strlen(arguments.rport) == 0) {
		strncpy(arguments.rport, arguments.port, MAX_DEV_STR_LEN);
	}

	lo_server_thread st;
	if (arguments.tcp) {
		st = lo_server_thread_new_with_proto(arguments.rport,LO_TCP, error);
	} else {
		st = lo_server_thread_new_with_proto(arguments.rport,LO_UDP, error);
	}

	/* add method that will match any path and args */
  //lo_server_thread_add_method(st, "/exctr/volume", "ii", exctr_volume, NULL);
	lo_server_thread_add_method(st, NULL, NULL, process_incoming_osc, NULL);
	lo_server_thread_start(st);
	if (arguments.verbose) {
		cout << "Waiting for OSC messages at port " << arguments.rport << " using";
		if (arguments.tcp)
			cout << " TCP." << endl;
		else
			cout << " UDP." << endl;
	}

	signal(SIGINT, exit_cli);
	signal(SIGTERM, exit_cli);

	run = true;

	struct stat	buffer;
	while (run) {
    /* Here we wait until ctrl-c or kill signal will be sent... */
		/* Here we check if serial devices are still online. */
		sleep(1);

		for (int i = 0; i < arguments.serialcount; i++) {

			string fullpath;
			fullpath += serialdevicebody;
			fullpath += convertInt(i+serialdevicenumber);

			if (serials[i] > 0 && stat (fullpath.c_str(), &buffer) != 0) {
				// Serial device has dropped
				if (arguments.respawn) {
					if (arguments.verbose)
						cout << fullpath << " (" << serials[i] <<"): disappeared!"  << endl;
					close(serials[i]);
					serials[i] = -1;
				}	else {
					run = false;
					if (arguments.verbose)
						cout << arguments.serialdevice << "(" << serials[i] <<"): disappeared! Exiting..."  << endl;
				}	
			}
			
			if (arguments.respawn && serials[i] == -1) {
				serials[i] = open_serial_device(fullpath);
			}
		}
	}

	/* Destruction routine */
	if (arguments.verbose)
		cout << '\r' << "Stopping [OSC]->[Serial] communication..." << endl;

	void* status;
  lo_server_thread_stop(st);
  lo_server_thread_free(st);
  usleep(1000); // Attempt to make sure that there is time to free the port. But might not help at all in this!
	if (arguments.verbose) 
		cout << "Morjens!";
	cout << endl;
}

