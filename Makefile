all:
	g++ -o exctr_osc_server ./src/exctr_osc_server.cpp -llo -pthread
clean:
	rm -f exctr_osc_server
install:
	mkdir -p $(DESTDIR)/usr/local/bin
	cp exctr_osc_server $(DESTDIR)/usr/local/bin
	cp udev/98-usb-exctr.rules /etc/udev/rules.d/
uninstall:
	rm -f $(DESTDIR)/usr/local/bin/exctr_osc_server
